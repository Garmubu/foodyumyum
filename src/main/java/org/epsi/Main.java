package org.epsi;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.expr;


public class Main {
    public static void main(String[] args) {
        SparkSession sparkSession= SparkSession.builder().appName("Open food fact").master("local[5]").getOrCreate();

        // Mise en place des dataframe des trois csv present dans le projet
        Dataset<Row> dataOpenFoodFact = sparkSession.read()
                .format("csv")
                .option("header", "true")
                .option("delimiter","\t")
                .load("src/main/resources/en.openfoodfacts.org.products.csv");

        Dataset<Row> dataPersonne = sparkSession.read()
                .format("csv")
                .option("header", "true")
                .option("delimiter",",")
                .load("src/main/resources/Liste_de_Personnes_par_Regime_Alimentaire.csv");

        Dataset<Row> dataRegime = sparkSession.read()
                .format("csv")
                .option("header", "true")
                .option("delimiter",",")
                .load("src/main/resources/Valeurs_Moyennes_par_Regime_Alimentaire.csv");

        dataOpenFoodFact.show();

        // suppression des doublon mais risque d'explosion de pc
        //Dataset<Row> CleanDf = dataOpenFoodFact.dropDuplicates("product_name");
        // Montre les en tete  avec leurs types
        dataOpenFoodFact.printSchema();

        // Liste des produits avec le nom du produit , la categorie ,les ingredients present ,la valeur nutritionnel
        Dataset<Row> listeProduit = dataOpenFoodFact.select(
                dataOpenFoodFact.col("product_name"),
                dataOpenFoodFact.col("categories_tags"),
                dataOpenFoodFact.col("ingredients_text"),
                dataOpenFoodFact.col("energy-kj_100g").cast("float"),
                dataOpenFoodFact.col("fat_100g").cast("float"),
                dataOpenFoodFact.col("sugars_100g").cast("float"),
                dataOpenFoodFact.col("fiber_100g").cast("float"),
                dataOpenFoodFact.col("proteins_100g").cast("float")
        );

        // Suppresion des valeur null et affichage de la nouvel liste de produit
        listeProduit = listeProduit.na().drop();
        listeProduit.show(100);

        // Filtrage selon regime alimentaire
        Dataset<Row> veganDf = listeProduit.filter(expr("NOT (categories_tags LIKE '%fish%' OR categories_tags LIKE '%dairies%' OR categories_tags LIKE '%meat%')"));

        Dataset<Row> pesceDf = listeProduit.filter(expr("NOT (categories_tags LIKE '%meat%')"));

        Dataset<Row> paleoDf = listeProduit.filter(expr("categories_tags LIKE '%fish%' OR categories_tags LIKE '%vegetable%' OR categories_tags LIKE '%meat%' OR categories_tags LIKE '%chicken%' OR categories_tags LIKE '%egg%' OR categories_tags LIKE '%honey%' OR categories_tags LIKE '%oil%' OR categories_tags LIKE '%tuber%' OR categories_tags LIKE '%rice%'"));

        Dataset<Row> glutenFreeDf = listeProduit.filter(expr("NOT (ingredients_text LIKE '%gluten%')"));


        //Remplacer les Varie par null
        for (String columnName : dataRegime.columns()){
            dataRegime = dataRegime.withColumn(columnName,
                    when(dataRegime.col(columnName).equalTo("Varie"), null).otherwise(dataRegime.col(columnName)));
        }
        dataRegime = dataRegime.na().drop();
        dataRegime.show();

        //on affiche nos clients
        dataPersonne.show();

        // Affiche les allergie

        dataPersonne.filter(expr("Allergie is not NULL")).show();

        //on fait l'attribution des menus a chaque personne selon leur regime et allergies
        Dataset<Row> platCompatible;
        JavaRDD<Row> rdd = dataPersonne.javaRDD();
        for(Row person : rdd.collect()){
            //attribution selon regime
            platCompatible = switch (person.get(2).toString()) {
                case "Sans gluten" -> glutenFreeDf;
                case "Végétarien", "Végétalien", "Vegan" -> veganDf;
                case "Paléo" -> paleoDf;
                case "Pescétarien" -> pesceDf;
                default -> listeProduit;
            };
            //dernier filtre sur l'allergie si non null
            if(person.get(7)!=null){
                platCompatible= platCompatible.filter(expr("NOT (ingredients_text LIKE '%"+person.get(7).toString().toLowerCase()+"%')"));
            }

            //call la fonction qui build le menus a partir de platCompatible
            creationMenu(platCompatible , person.get(2).toString(), person.get(0).toString());

            //libere de l'espace memoire en vidant le dataframe
            platCompatible.unpersist();
        }

    }
    // Fonction permettant de creer un menu complet pour chaque jours de la semaine - EXTREMEMENT LONG, ne pas hesiter a desactiver
    public static void creationMenu(Dataset<Row> menu , String regime , String nom){
        List<String> listeMenu = new ArrayList<>();

        // joujable selon la puissance de votre machine
        int valeurLimitant = 10 ;

        // Different liste representant le menu d'une journée on a choisit une limite de 200 parce que le traitement est bien trop lents si on prends toutes les données
        List<Row> plat = menu.filter(expr("categories_tags LIKE '%meats%' OR categories_tags LIKE '%plant-based-food%' ")).select("product_name").limit(valeurLimitant).collectAsList();
        List<Row> snacks = menu.filter(expr("categories_tags LIKE '%snacks%'")).select("product_name").limit(valeurLimitant).collectAsList();
        List<Row> desserts = menu.filter(expr("categories_tags LIKE '%desserts%'")).select("product_name").limit(valeurLimitant).collectAsList();
        List<Row> condiments = menu.filter(expr("categories_tags LIKE '%condiments%'")).select("product_name").limit(valeurLimitant).collectAsList();



        //Boucle permettant de repeter l'operation sur 7 jour pour genere un menu hebdomadaire aleatoire
        for (int i = 0; i < 7 ; i++){
            listeMenu.add("Plat  : "+ plat.get((int)(Math.random() * valeurLimitant)).toString() + " ,Snacks : "+ snacks.get((int)(Math.random() * valeurLimitant)).toString() +" , Desserts : "+ desserts.get((int)(Math.random() * valeurLimitant)).toString() +" , Condiments :"+condiments.get((int)(Math.random() * valeurLimitant)).toString());
            // affiche les menus jours par jours
            System.out.println(listeMenu.get(i));
        }
        System.out.println("Menu de la semaine pour " + nom + " avec comme regime " + regime );
    }
}